﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody2D rb;
    
    public float runSpeed;
    public float jumpForce;
    public SpriteRenderer spriteRenderer;
    public Animator animator;
    public Text coinText;
    private int numGems = 0;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        coinText.text = "Gems Collected: 0";
    }

    void Update()
    {
        if(Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");

            if(Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, .01f, levelMask))
            {
                Jump();
            }
        }
    }

    void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        
        rb.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rb.velocity.y);

        if(rb.velocity.x >= 0)
        {
            spriteRenderer.flipX = false;
        }
        else
        {
            spriteRenderer.flipX = true;
        }

        /*
        if(rb.velocity.x != 0)
        {
            animator.SetBool("isMoving", true);
        }
        else
        {
            animator.SetBool("isMoving", false);
        }
        */
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Gem")
        {
            other.gameObject.SetActive(false);
            numGems += 1;

            coinText.text = "Gems Collected: " +numGems;
        }
        
        if(other.tag == "Enemy")
        {
            SceneManager.LoadScene("2D Project");
        }

        if(other.gameObject.name == "Hitbox")
        {
            other.gameObject.transform.parent.gameObject.SetActive(false);
        }
    }

    void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
    }
}
